package Pages;

import lib.Driver;
import lib.Element;
import lib.Page;

public class GooglePage extends Page {
    
    public Element inputSearch;
    public Element resultStats;

    public GooglePage(Driver driver) {
        super(driver);
        super.driver.Navigate("https://www.google.com/");
        InitComponents();
    }
    
    private void InitComponents(){
        inputSearch = new Element(this, ".gLFyf");
        resultStats = new Element(this, "#resultStats");
    }
    
}
