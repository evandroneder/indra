package lib;

import java.util.ArrayList;
import java.util.Date;

public class Diretor {
    
    public String nome;
    public Date nascimento;
    public ArrayList<Filme> filmes;
    
    public Diretor(String nome, Date nascimento){
        this.nome = nome;
        this.nascimento = nascimento;
        filmes = new ArrayList<>();
    }
    
    public void AdicionarFilme(String nome, Date lancamento){
        filmes.add(new Filme(nome, lancamento));
    }
}
