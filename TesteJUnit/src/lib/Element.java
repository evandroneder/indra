package lib;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class Element {
    
    private Page _page;
    private String _selector;
    private WebElement _element;
    
    public Element(Page page, String selector){
        _page = page;
        _selector = selector;
    }
    
    public String GetValue(){
        LoadElement();
        return _element.getAttribute("value");
    }
    
    public void SetValue(String value, String Key){
        LoadElement();
        _element.sendKeys(value);
        switch(Key.toUpperCase()){
            case "ENTER": _element.sendKeys(Keys.RETURN);
        }
    }
    
    public String GetText(){
        LoadElement();
        return _element.getText();
    }
    
    private void LoadElement(){
        _element = _page.driver.GetElementByCss(_selector); 
    }
}
