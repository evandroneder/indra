package lib;

public class Page{
    
    public Driver driver;
    
    public Page(Driver driver){
        this.driver = driver;
    }
    
    public Element GetElement(String selector){
        return new Element(this, selector);
    }
    
    public Page Navigate(String url){
        driver.Navigate(url);
        return new Page(driver);
    }
}
