
package lib;

import java.util.Date;


public class Filme {
    
    public String nome;
    public Date lancamento;
    
    public Filme(String nome, Date lancamento){
        this.nome = nome;
        this.lancamento = lancamento;
    }
}
