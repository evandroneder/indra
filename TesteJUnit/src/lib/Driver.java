package lib;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {
  
    private WebDriver driver;
    
    public Driver(){
        System.setProperty("webdriver.chrome.driver", "c:\\Windows\\chromedriver.exe");
        driver = new ChromeDriver();
    }
    
    public void Close(){
        driver.close();
        driver.quit();
    }
    
    public void Navigate(String url){
        driver.get(url);
    }
    
    public WebElement GetElementByCss(String selector){
        return driver.findElement(By.cssSelector(selector));
    }
}