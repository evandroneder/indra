package teste;

import Pages.GooglePage;
import java.util.Date;
import lib.Diretor;
import lib.Driver;
import lib.Filme;
import org.junit.After;
import org.junit.Test;
import org.junit.Before;

public class MyJUnitTest {
    
    private Driver driver;
    
    public MyJUnitTest() {
         driver = new Driver();
    }
    
    @Before
    public final void before() { 
    }
    
    
    @After
    public final void after() { 
        driver.Close();
    }
    
    @Test
    public void GoogleTest(){
        
        Diretor diretor = new Diretor("Chris Columbus", new Date("09/10/1958"));
        diretor.AdicionarFilme("Harry Potter and the Chamber of Secrets", new Date("11/02/2002"));
       
        GooglePage googlePage = new GooglePage(driver);
        
        for(Filme filme: diretor.filmes){
            googlePage.inputSearch.SetValue(filme.nome, "ENTER");
            String googleResult = googlePage.resultStats.GetText();
         }
    }
}
