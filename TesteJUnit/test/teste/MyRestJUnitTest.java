/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste;

import org.junit.Test;

/**
 *
 * @author Evandro
 */
public class MyRestJUnitTest {
    
    public MyRestJUnitTest() {
    }

    @Test
    public void RestTest(){
        given()
		 .when()
		    .get("https://jsonplaceholder.typicode.com/todos/1")
		 .then()
		    .statusCode(200)
		    .body("userId", is(1))
		    .body("id", is(1))
		    .body("title", equalTo("delectus aut autem"))
		    .body("completed", equalTo("false"))
		    .assertThat();
    }
}
